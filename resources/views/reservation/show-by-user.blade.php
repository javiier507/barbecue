@extends('layouts.master')

@section('title')
    Reservaciones
@endsection

@section('content')

    <section class="section">
        <div class="container">
            <h1 class="title">Barbacoas Reservadas</h1>
        </div>
    </section>

    <div class="container">
        @foreach ($reservations->chunk(4) as $chunk)
            <div class="columns">
                @foreach ($chunk as $item)
                    <div class="column is-3">
                        <div class="card custom-card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="{{ $item->barbecue->image_url }}" alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-left">
                                        <figure class="image is-48x48">
                                            <img src="{{ asset('images/user.png') }}" alt="Placeholder image">
                                        </figure>
                                    </div>
                                    <div class="media-content">
                                        <p class="title is-4">{{ $item->barbecue->name }}</p>
                                        <p class="subtitle is-6">{{ $item->barbecue->owner->name }}</p>
                                    </div>
                                </div>

                                <div class="content">
                                    <p><time>{{ $item->date }}</time></p>
                                    <p><time>{{ $item->hour_start }} - {{ $item->hour_end }}</time></p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>

@endsection