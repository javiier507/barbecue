<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name')) | Barbacoas</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

        <nav class="navbar is-dark container" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarBasicExample" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="/">
                        inicio
                    </a>
                    @auth
                        <a href="{{ route('reservations.byUser') }}" class="navbar-item">Mis Reservaciones</a>
                        <a href="{{ route('barbecues.byUser') }}" class="navbar-item">Mis Barbacoas</a>
                        <a href="{{ route('barbecues.create') }}" class="navbar-item">Agregar Barbacoa</a>
                    @endauth
                </div>

                <div class="navbar-end">
                    <div class="navbar-item">
                        <div class="buttons">
                            @guest
                                <a class="button is-light" href="{{ route('login') }}">
                                    <strong>{{ __('Login') }}</strong>
                                </a>
                            @else
                                <a class="button is-light">
                                    {{ Auth::user()->name }}
                                </a>
                                <a class="button is-light" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            @endguest
                        </div>
                    </div>
                </div>
            </div>
        </nav>

        <div>
            @yield('content')
        </div>

        <footer class="footer">
            <div class="content has-text-centered">
                <p>
                    Por <strong>Carlos Peñalba. </strong><a href="https://bulma.io">Construido Con Bulma.</a> 2019
                </p>
            </div>
        </footer>

    </div>
</body>
</html>