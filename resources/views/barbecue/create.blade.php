@extends('layouts.master')

@section('title')
    Agregar Barbacoa
@endsection

@section('content')
    <div class="container">
        <barbecue-form-component></barbecue-form-component>
    </div>
@endsection