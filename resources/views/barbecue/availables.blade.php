@extends('layouts.master')

@section('title')
    Barbacoas Disponibles
@endsection

@section('content')
    <barbecue-available
            route-available="{{ route('barbecues.api.availables') }}"
            route-barbecue="{{ route('barbecues.index') }}">
    </barbecue-available>
@endsection