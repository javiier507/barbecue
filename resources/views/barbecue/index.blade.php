@extends('layouts.master')

@section('title')
    Inicio
@endsection

@section('content')

    <section class="hero is-large container custom-hero">
        <div class="hero-body">
            <div class="container">
                <h1 class="title has-text-weight-bold has-text-light is-size-1">Encuentra una Barbacoa cerca de ti</h1>
                <a href="{{ route('barbecues.availables') }}" class="button is-light">Buscar</a>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Barbacoas Recien Agregadas</h1>
            <h2 class="subtitle">
                Comparte agradables momentos con amigos y familiares. Elige la Barbacoa que más te guste.
            </h2>
        </div>
    </section>

    <div class="container">
        @foreach ($barbecues->chunk(4) as $chunk)
            <div class="columns">
                @foreach ($chunk as $item)
                    <div class="column is-3">
                        <div class="card custom-card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="{{ $item->image_url }}" alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-left">
                                        <figure class="image is-48x48">
                                            <img src="{{ asset('images/user.png') }}" alt="Placeholder image">
                                        </figure>
                                    </div>
                                    <div class="media-content">
                                        <p class="title is-4">
                                            <a class="has-text-dark" href="{{ route('barbecues.show', ['barbecue' => $item->hash]) }}">
                                                {{ $item->name }}
                                            </a>
                                        </p>
                                        <p class="subtitle is-6">{{ $item->owner->name }}</p>
                                    </div>
                                </div>
                                <div class="content">
                                    <p>{{ \Illuminate\Support\Str::limit($item->description, 150) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>

@endsection