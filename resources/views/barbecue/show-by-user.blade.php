@extends('layouts.master')

@section('title')
    Barbacoas Agregadas
@endsection

@section('content')

    <section class="section">
        <div class="container">
            <h1 class="title">Lista de tus Barbacoas</h1>
        </div>
    </section>

    <div class="container">
        @foreach ($barbecues->chunk(4) as $chunk)
            <div class="columns">
                @foreach ($chunk as $item)
                    <div class="column is-3">
                        <div class="card custom-card">
                            <div class="card-image">
                                <figure class="image is-4by3">
                                    <img src="{{ $item->image_url }}" alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="card-content">
                                <div class="media">
                                    <div class="media-left">
                                        <figure class="image is-48x48">
                                            <img src="{{ asset('images/user.png') }}" alt="Placeholder image">
                                        </figure>
                                    </div>
                                    <div class="media-content">
                                        <p class="title is-4">{{ $item->name }}</p>
                                    </div>
                                </div>

                                <div class="content">
                                    <p>{{ \Illuminate\Support\Str::limit($item->description, 150) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>

@endsection