@extends('layouts.master')

@section('title')
    Reservar Barbacoa
@endsection

@section('content')
    <div class="container">
        <div class="columns">
            <div class="column is-4">
                <div class="card">
                    <div class="card-image">
                        <figure class="image is-4by3">
                            <img src="{{ $barbecue->image_url }}" alt="Placeholder image">
                        </figure>
                    </div>
                    <div class="card-content">
                        <div class="media">
                            <div class="media-left">
                                <figure class="image is-48x48">
                                    <img src="{{ asset('images/user.png') }}" alt="Placeholder image">
                                </figure>
                            </div>
                            <div class="media-content">
                                <p class="title is-4">{{ $barbecue->name }}</p>
                                <p class="subtitle is-6">{{$barbecue->owner->name  }}</p>
                            </div>
                        </div>

                        <div class="content">
                            <p>{{ $barbecue->description  }}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column is-8">
                <reservation-form-component barbecue="{{ $barbecue->hash }}"></reservation-form-component>
            </div>
        </div>
    </div>
@endsection