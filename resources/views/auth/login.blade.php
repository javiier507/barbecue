@extends('layouts.master')

@section('title')
    Login
@endsection

@section('content')
<div class="container">
    <div class="columns">
        <div class="column is-4 is-offset-4">
            <div class="box">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="field">
                        <label for="email" class="label">{{ __('E-Mail Address') }}</label>

                        <div class="control">
                            <input id="email" type="email" class="input" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="field">
                        <label for="password" class="label">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="input" name="password" required autocomplete="current-password">

                            @error('password')
                                <span role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="field">
                        <input class="checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="labell" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>

                    <div class="field">
                        <button type="submit" class="button is-info">
                            {{ __('Login') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
