<?php

namespace App\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Vinkla\Hashids\Facades\Hashids;

class Barbecue extends Model
{
    protected $fillable = ['name', 'description', 'image', 'coordinates', 'user_owner'];
    protected $hidden = ['id', 'coordinates', 'created_at', 'updated_at'];
    protected $appends = ['hash', 'lat_lng'];

    /*
     * relationships
     */

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_owner');
    }

    /*
     * accessors
     */

    public function getHashAttribute()
    {
        return Hashids::encode($this->id);
    }

    public function getLatLngAttribute()
    {
        //  Specific for MYSQL

        $latLng = DB::table('barbecues')
            ->select(DB::raw("CONCAT(ST_X(coordinates), ',', ST_Y(coordinates)) AS latLng"))
            ->where('id', '=', $this->id)
            ->get();

        if(count($latLng) == 1 && property_exists($latLng[0], 'latLng')) {
            $latLng= $latLng[0]->latLng;
        } else {
            $latLng = '';
        }

        return $latLng;
    }

    public function getImageUrlAttribute()
    {
        $url = 'https://bulma.io/images/placeholders/1280x960.png';
        if(Storage::disk(env('STORAGE_DISK'))->exists($this->image)) {
            $url = Storage::disk(env('STORAGE_DISK'))->url($this->image);
        }
        return $url;
    }

    public static function getImageUrlHelper($image)
    {
        $url = 'https://bulma.io/images/placeholders/1280x960.png';
        if(Storage::disk(env('STORAGE_DISK'))->exists($image)) {
            $url = Storage::disk(env('STORAGE_DISK'))->url($image);
        }
        return $url;
    }

    /*
     * key route model binding
     */

    public function getRouteKeyName()
    {
        return 'hash';
    }
}
