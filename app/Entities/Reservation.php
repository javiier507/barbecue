<?php

namespace App\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Carbon;

class Reservation extends Model
{
    protected $fillable = ['date', 'hour_start', 'hour_end', 'barbecue_id', 'user_client'];

    /*
     * relationships
     */

    public function barbecue()
    {
        return $this->belongsTo(Barbecue::class, 'barbecue_id');
    }

    public function client()
    {
        return $this->belongsTo(User::class, 'user_client');
    }

    /*
     * Mutators
     */

    public function setDateAttribute($value)
    {
        $this->attributes['date'] = Carbon::createFromTimeString($value);
    }

    public function setHourStartAttribute($value)
    {
        $this->attributes['hour_start'] = Carbon::createFromTimeString($value)
            ->setTimezone(env('LOCAL_TIMEZONE'));
    }

    public function setHourEndAttribute($value)
    {
        $this->attributes['hour_end'] = Carbon::createFromTimeString($value)
            ->setTimezone(env('LOCAL_TIMEZONE'));
    }

    public static function setDateTimeFormat($value)
    {
        return Carbon::createFromTimeString($value)
            ->setTimezone(env('LOCAL_TIMEZONE'))
            ->toDateTimeString();
    }
}
