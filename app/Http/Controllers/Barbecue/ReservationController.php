<?php

namespace App\Http\Controllers\Barbecue;

use App\Entities\Reservation;
use App\Http\Requests\ReservationFormRequest;
use App\Services\ReservationService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReservationController extends Controller
{
    private $reservationService;

    public function __construct(ReservationService $reservationService)
    {
        $this->reservationService = $reservationService;
    }

    public function index()
    {

    }

    public function store(ReservationFormRequest $request)
    {
        $client = $request->user();
        $data = $request->input();
        $result = false;

        try{
            $result = $this->reservationService->save($data, $client);
        }catch (\Exception $exception)
        {
            $message = 'Lo sentimos, ha ocurrido un problema';
            $code = 500;
            if($exception->getCode() == 400) {
                $message = $exception->getMessage();
                $code = $exception->getCode();
            }
            return response()->json(['body' => $message], $code);
        }

        return response()->json($request->all(), 201);
    }

    public function showByUser()
    {
        $authId = Auth::id();
        $reservations = Reservation::with(['barbecue'])->where('user_client', $authId)->paginate(10);
        return view('reservation.show-by-user', compact('reservations'));
    }
}
