<?php

namespace App\Http\Controllers\Barbecue;

use App\Entities\Barbecue;
use App\Http\Controllers\Controller;

use App\Services\BarbecueService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BarbecueController extends Controller
{
    private $barbecueService;

    public function __construct(BarbecueService $barbecueService)
    {
        $this->barbecueService = $barbecueService;
    }

    public function index()
    {
        $barbecues = $this->barbecueService->getAll(8);
        return view('barbecue.index', compact('barbecues'));
    }

    public function show(Barbecue $barbecue)
    {
        return view('barbecue.show', compact('barbecue'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $image = $request->file('image');
        //$fileName = $image->getFilename() . '-' . time() . '.' .$image->getClientOriginalExtension();
        $fileName =  Storage::disk(env('STORAGE_DISK'))->put('', $image, 'public');
        //$image->storeAs(env('STORAGE_DISK'), $fileName);

        $data = $request->input();
        $coordinates = $data['coordinates'];    

        $data['image'] = $fileName;
        $data['user_owner'] = Auth::id();
        $data['coordinates'] = DB::raw("POINT($coordinates)");

        $data = $this->barbecueService->create($data);

        return response()->json($data, 201);
    }

    public function showByUser()
    {
        $authId = Auth::id();
        $barbecues = $this->barbecueService->getbyUser($authId, 25);
        return view('barbecue.show-by-user', compact('barbecues'));
    }

    public function create()
    {
        return view('barbecue.create');
    }

    public function getAvailable(Request $request)
    {
        $ubication = $request->query('ubication');
        $km = $request->query('meters', 5000);

        $barbecues = $this->barbecueService->listByDistance($ubication, $km, 8);
        return response()->json($barbecues, 200);
    }

    public function showAvailable()
    {
        return view('barbecue.availables');
    }
}
