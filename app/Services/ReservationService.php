<?php
/**
 * Created by PhpStorm.
 * User: carlospenalba
 * Date: 6/25/19
 * Time: 4:34 AM
 */

namespace App\Services;

use App\Entities\Reservation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Vinkla\Hashids\Facades\Hashids;

class ReservationService
{
    private $barbecueService;

    public function __construct(BarbecueService $barbecueService)
    {
        $this->barbecueService = $barbecueService;
    }

    private function prepare($data)
    {
        $data['barbecue'] = Hashids::decode($data['barbecue'])[0];
        $data['date'] = Reservation::setDateTimeFormat($data['date']);
        $data['hour_start'] = Reservation::setDateTimeFormat($data['hour_start']);
        $data['hour_end'] = Reservation::setDateTimeFormat($data['hour_end']);
        return $data;
    }

    public function validTime($data)
    {
        $data = $this->prepare($data);

        $result = Reservation::where('barbecue_id', $data['barbecue'])->count();
        if($result === 0)
        {
            $result = 1; // FALSO POSITIVO
        }
        else
        {
            $result =  DB::table('reservations')
                ->whereRaw('
                (
                    ((hour_start > ? AND hour_start > ?) OR (hour_end < ? AND hour_end < ?))
                        AND
                    (date = ?)
                        AND
                    (barbecue_id = ?)
                )
                OR
                (
                    ((hour_start > ? AND hour_start > ?) OR (hour_end < ? AND hour_end < ?))
                        AND
                    (date < ?)
                        AND
                    (barbecue_id = ?)
                )
                OR
                (
                    ((hour_start > ? AND hour_start > ?) OR (hour_end < ? AND hour_end < ?))
                        AND
                    (date > ?)
                        AND
                    (barbecue_id = ?)
                )
            ', [
                    $data['hour_start'], $data['hour_end'], $data['hour_start'], $data['hour_end'],
                    $data['date'],
                    $data['barbecue'],
                    //
                    $data['hour_start'], $data['hour_end'], $data['hour_start'], $data['hour_end'],
                    $data['date'],
                    $data['barbecue'],
                    //
                    $data['hour_start'], $data['hour_end'], $data['hour_start'], $data['hour_end'],
                    $data['date'],
                    $data['barbecue'],
                ])
                ->count('id');
        }
        return $result;
    }

    public function save($data, $client)
    {
        $result = false;
        try
        {
            $barbecue = $this->barbecueService->getSingle($data['barbecue']);

            if(is_null($client) || is_null($barbecue)) {
                throw new \Exception('Datos de Cliente o Barbacoa Incorrectos', 400);
            }

            $valid = $this->validTime($data);
            if($valid == 0) {
                throw new \Exception('Horario no disponible', 400);
            }

            if($barbecue->user_owner === $client->id) {
                throw new \Exception('No es posible rentar su propia barbacoa', 400);
            }

            $reservation = new Reservation();
            $reservation->date = $data['date'];
            $reservation->hour_start = $data['hour_start'];
            $reservation->hour_end =  $data['hour_end'];
            $reservation->barbecue()->associate($barbecue);
            $reservation->client()->associate($client);
            $result = $reservation->save();
        }
        catch (\Exception $exception)
        {
            throw $exception;
        }
        return $result;
    }
}