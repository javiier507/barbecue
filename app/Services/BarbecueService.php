<?php
/**
 * Created by PhpStorm.
 * User: carlospenalba
 * Date: 6/22/19
 * Time: 9:29 PM
 */

namespace App\Services;

use App\Entities\Barbecue;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Support\Facades\DB;

class BarbecueService
{
    public function getAll($rows)
    {
        return Barbecue::with('owner')->orderBy('id', 'desc')->paginate($rows);
    }

    public function getSingle($hash)
    {
        return Barbecue::find(Hashids::decode($hash))->first();
    }

    public function getbyUser($user, $rows)
    {
        return Barbecue::where('user_owner', $user)->orderBy('id', 'desc')->paginate($rows);
    }

    public function create($data)
    {
        return Barbecue::create($data);
    }

    public function listByDistance($ubication, $meters, $rows)
    {
        $results = DB::table('barbecues')
            ->select(
                'id',
                'name', 
                'description', 
                'image'
            )
            ->selectRaw("CONCAT(ST_Y(coordinates), ',', ST_X(coordinates)) as latlng")
            ->selectRaw("ROUND((ST_Distance_Sphere(POINT(ST_Y(coordinates), ST_X(coordinates)), POINT($ubication))/1000), 2) as distance")
            ->whereRaw("(ST_Distance_Sphere(POINT(ST_Y(coordinates), ST_X(coordinates)), POINT($ubication)) < ?)", [$meters])
            ->orderBy('id', 'desc')
            ->paginate($rows);

        $results->getCollection()->transform(function ($value) {
            $value->id = Hashids::encode($value->id);
            $value->image = Barbecue::getImageUrlHelper($value->image);
            return $value;
        });

        return $results;
    }
}