<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

//  Route::get('/home', 'HomeController@index')->name('home');

/*
 * Barbecue Routes
 */

Route::group([
    'namespace' => 'Barbecue'
], function () {
    Route::get('/', 'BarbecueController@index');

    Route::group([
        'middleware' => 'auth'
    ], function () {
        Route::resource('barbecues', 'BarbecueController');
        Route::get('my-barbecues', 'BarbecueController@showByUser')->name('barbecues.byUser');
        Route::get('api/barbecues/availables', 'BarbecueController@getAvailable')->name('barbecues.api.availables');
        Route::get('availables', 'BarbecueController@showAvailable')->name('barbecues.availables');

        Route::resource('reservations', 'ReservationController');
        Route::get('/my-reservations', 'ReservationController@showByUser')->name('reservations.byUser');
    });
});

