<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarbecuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barbecues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->string('description', 2555);
            $table->string('image', 255);
            $table->point('coordinates');
            $table->unsignedBigInteger('user_owner');
            $table->foreign('user_owner')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->time('hour_start');
            $table->time('hour_end');
            $table->unsignedBigInteger('barbecue_id');
            $table->foreign('barbecue_id')->references('id')->on('barbecues')->onDelete('cascade');
            $table->unsignedBigInteger('user_client');
            $table->foreign('user_client')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
        Schema::dropIfExists('barbecues');
    }
}
