<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\Barbecue;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(Barbecue::class, function (Faker $faker) {

    $coords = [
        'POINT(8.947411, -79.675159)',  // Paseo Arraijan
        'POINT(8.895029, -79.752454)',  //  Costa Verde
        'POINT(8.889570, -79.749175)',  // Market Plaza
        'POINT(9.009653, -79.536281)',  // El Dorado
        'POINT(9.013734, -79.512128)',  //  12 de Octubre
        'POINT(9.013734, -79.512128)',  //  San Fransisco
        'POINT(8.977808, -79.520561)',  //  Marbella
        'POINT(8.990529, -79.519392)',  //  Obarrio
        'POINT(8.991065, -79.529136)',  //  El Cangrejo
        'POINT(8.977870, -79.508357)',  //  Punta Pacifica
    ];

    return [
        'name' => $faker->name,
        'description' => $faker->sentence(20),
        'image' => 'https://bulma.io/images/placeholders/1280x960.png',
        'coordinates' => DB::raw($coords[$faker->numberBetween(0, count($coords)-1)]),
        'user_owner' => \App\User::all()->random()
    ];
});
